# fechahora

**(no se ven las imagenes, se creo un readme.pdf, ahi esta completo la explicacion del readme.md)**



**ISTEA - Tecnicatura Superior en Soporte de Infraestructuras TI**

**Fecha: 07/12/2021**

**Materia: Infraestructura de Servidores**

**Profesor: Sergio Pernas**

**Autor: Nestor Jorge Urfajg**

# TP Final

## Consignas

**Dockerfile**

Crear una imagen Docker que puede contener:

- Una aplicación de desarrollo propio (dinámica o html estático).
- Una aplicación de Docker Hub, la imagen original debe ser modificada para con
  características que no posea la original, por ejemplo, usar una imagen de Wordpress para
  crear una imagen nueva de Wordpress pero que incluya plugins o nuevos themes.
- Implementar una aplicación de terceros partiendo de una imagen base como por ejemplo
  Ubuntu.
- Una imagen orientada a herramientas en el ámbito de administración de sistemas e
  infraestructuras, por ejemplo, un servicio DNS, proxy, analizador de logs, etc.

**Repositorio GIT**

Crear un repositorio GIT para el proyecto a entregar. El repositorio debe contener:

- Dockerfile.
- Ficheros necesarios para construir la imagen.
- Incluir el fichero README.md

**README.md**

El README debe incluir:

- Explicación de cada aspecto volcado en el Dockerfile.
- Explicación de los scripts, si los hubiere.
- Explicación de ficheros de configuración, si los hubiere.
- Explicación de los pasos para construir la imagen.
- Pasos para hacer el deploy del contenedor en distintas modalidades (con volúmenes, con
  variables de entorno, etc.), si la imagen lo permite.

**Presentación oral**

En la instancia oral se evaluará:

- Los fundamentos de 'el qué' y el 'por qué' de lo volcado en el proyecto.
- Modelo de capas IaaS, PaaS, SaaS.
- Los temas vistos en clases y volcados en los 'pads'

## Objetivo

Iniciar un servicio web con nginx para mostrar un sitio web mediante contenedor Docker.

## Alcance

Se indicarán los pasos para:

1. La instalación y configuración inicial del sistema operativo.
2. Instalación de Docker y creación de imagen con Dockerfile que contendrá los archivos necesarios para iniciar el servicio nginx en un contenedor Docker.
3. Por último se indicarán los pasos para crear el contenedor y mostrar un sitio web estático.

## Instalación y configuración inicial de sistema operativo Ubuntu 18.04

Se ha instalado el sistema operativo en una máquina virtual de Virtualbox. 

Instalado el sistema operativo e iniciado sesión en este, Se procedio con la configuración inicial:

* Un nombre de equipo o hostname que lo identifique.
* Una IP fija
* Reglas de firewall

### Hostname o nombre de equipo

Para definir un hostname se utilizo el comando `$ sudo hostnamectl set-hostname [nombre del equipo]`. Ejemplo `$ sudo hostnamectl set-hostname docker`

Para aplicar el cambio se reinicio el equipo con el comando `$ sudo reboot`

### Definir una IP fija

Primero con el siguiente comando se actualizo la lista de paquetes disponibles y al mismo tiempo se instalo el editor de texto Vim que nos permitio editar archivos de configuración del sistema en este caso el de red 

`$ sudo apt update &&  apt install vim -y`

Instalado VIM editamos el archivo de configuración de red `01-netcfg.yaml` con el siguiente comando: 

$ sudo vim /etc/netplan/01-netcfg.yaml`

Presionamos `i` para ingresar en modo edición y realizamos los cambios de tal manera que quede dhcp desactivado y la dirección fija de acuerdo a nuestra red local. Hay que tener en cuenta que este archivo de configuración está escrito en lenguaje `yaml` por lo tanto hay que asegurarse de que en cada renglón esté ingresada correctamente la sangría. Si hay espacios de más o de menos el sistema no reconocerá los datos ingresados.

```
# This file describes the network interfaces available on your system
# For more information, see netplan(5).
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: no
      addresses: [192.168.0.222/24]
      gateway4: 192.168.0.1
      nameservers:
              addresses: [8.8.8.8,8.8.4.4]
```

Para guardar los cambios hay que presionar `Esc` para salir del modo de edición y pasar al modo de comando.  Luego presionamos `:wq` para guardar y salir.

Para aplicar los cambios en el sistema se debe ejecutar el comando: `$ sudo netplan apply`. De no haber errores entonces lo habremos realizado correctamente.

### Definir las reglas del firewall

Para configurar el firewall se utilizo el comando `ufw`.

Con el comando `$ sudo ufw status` vemos el estado del firewall. Por defecto se encuentra inactivo.

Comenzamos deshabilitando todo el tráfico entrante: `$ sudo ufw default deny incoming`

Habilitar todo el tráfico saliente: `$ sudo ufw default allow outgoing`

Habilitar ssh: `$ sudo ufw allow ssh`

Activamos el firewall `$ sudo ufw enable`

El sistema preguntará

````
El comando puede interrumpir las conexiones ssh existentes. ¿Continuar con la operación (s|n)?
````

Seleccionamos **s** y nos mostrará

```
El cortafuegos está activo y habilitado en el arranque del sistema
```

Habilitar 'http' (puerto 80): `$ sudo ufw allow http`

Habilitar 'https' (puerto 443): `$ sudo ufw allow https`

## Instalación de Docker

Para este paso se requiere tener permisos de superusuario por lo tanto se utilizará el usuario root, dado que con `sudo` se pueden ejecutar los comandos que se indicarán a continuación pero aparecen los siguientes problemas de permisos en el directorio `/var/lib/dpkg/lock-fronted`.

Instalar y configurar Docker desde repositorio oficial:

`# apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y`

Descargar e importar llave pública de Docker

`# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -`

Agregar repositorio `apt` Docker

```
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# apt update
```

Instalar Docker

`# apt install docker-ce docker-ce-cli containerd.io`

Para evitar utilizar `$ sudo` en cada comando de Docker podemos agregar nuestro usuario al grupo 'docker' con el siguiente comando:

`# gpasswd -a 'usuario' docker`

## Crear de imagen con Dockerfile

Dockerfile es un archivo de configuración que automatiza los pasos para construir una imagen Docker. Es similar a Makefile. Docker lee instrucciones del Dockerfile para automatizar los pasos que de otro modo se realizarían manualmente para crear una imagen. Cuando se haya creado el Dockerfile, llame al comando `docker build`, utilizando la ruta del directorio que contiene Dockerfile como argumento.

Con el comando `$ vim Dockerfile` proceder a crear el fichero Dockerfile (debe ser escrito exactamente así).

Dentro del fichero ingresar los siguientes parámetros y guardar cambios.



![image-20211205170445965](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205170445965.png)

```

```

Instrucción `FROM`indica una imagen base para crear una nueva imagen.

Instrucción `LABEL` pasa información acerca de la imagen.

Instrucción `ARG` define variables de entorno dentro del nuevo contenedor para crear la imagen. La variable `DEBIAN_FRONTEND` le pasa el tipo de ejecución a los programas que lo soliciten. En este caso se le indica que será una instalación desatendida del sistema base.

Instrucción `RUN` ejecuta un comando dentro del contenedor temporal.

Instrucción `ADD` se encarga de copiar los ficheros y directorios desde una ubicación especificada y los agrega al sistema de ficheros del contenedor.

Instrucción `EXPOSE` expone los puertos del contenedor.

Instrucción `CMD` indica qué comando se ejecutará cuando el contenedor se inicie.

El próximo paso consiste en generar los ficheros  `default, entrypoint.sh e index.html`en el mismo directorio donde se encuentra el fichero `Dockerfile`.

Para esto se requiere generar el fichero `default` con los siguientes parametros:

![image-20211205170926365](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205170926365.png)

Donde se indica principalmente puerto de escucha y directorio raíz donde se guardará el fichero index.html, entre otros parámetros necesarios para la ejecución del servicio nginx.

Tambien se requiere generar el ejecutable  `entrypoint.sh ` con las siguientes instrucciones:

![image-20211205171539974](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205171539974.png)

Luego generar el fichero `index.html` con contenido estático que será el sitio que mostrará el servicio web en nuestro navegador.

***Nota: todos los ficheros necesarios para la creación de la imagen deben estar en el mismo directorio que el fichero Dockerfile.***

Una vez creado el fichero Dockerfile y generados todos los ficheros necesarios para la creación de la imagen procedemos a crear la imagen con el comando `$ docker build -t fechahora .` donde el parámetro -t  define un nombre y opcionalmente una etiqueta a la imagen en el formato `nombre:etiqueta`.

![image-20211205172001184](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205172001184.png)

Al finalizar la creación de la imagen se verá lo siguiente:

![image-20211205172156388](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205172156388.png)

Comando para ver las imágenes creadas: `$ docker images`

![image-20211205172337113](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205172337113.png)

## Crear un contenedor y acceder al sitio web

Crear un contenedor a partir de la imagen creada

`$ docker run -d - p 8080:80 -e SITENAME="Fecha y Hora Actual" --name fechahora fechahora`

![image-20211205173621185](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205173621185.png)

> 

Con el comando `$ docker ps` se podrá ver el estado de los contenedores que se están ejecutando.

![image-20211205173455158](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205173455158.png)

Finalmente resta verificar el acceso al sitio web desde el navegador ingresando la IP del host y el puerto definido en la ejecución del contenedor: http://192.168.0.222:8080/

![image-20211205173033939](C:\Users\NJU\AppData\Roaming\Typora\typora-user-images\image-20211205173033939.png)

> 

Asimismo se creo otro contenedor a partir de otra imagen creada

`$ docker run -d - p 8081:80 -e SITENAME="Fecha y Hora Actual 2" --name fechahora2 fechahora`

![image-20211205173954896](C:\Users\NJU\Pictures\image-20211205173954896.png)



Finalmente resta verificar el acceso al sitio web desde el navegador ingresando la IP del host y el puerto definido en la ejecución del contenedor: http://192.168.0.222:8081/



![image-20211205174127075](C:\Users\NJU\Pictures\image-20211205174127075.png)

## Conclusión

Este instructivo se generó con la intención de dar a conocer la creación de una imagen sencilla de forma automatizada, el funcionamiento básico de Docker y el despliegue de un servicio web nginx para servir una aplicación web estática.

